﻿using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyContactsApi.Service
{
    public interface IContactService
    {
        Task<ContactDTO> GetContact(string id);
        Task<List<ContactDTO>> GetUserContacts(string id);
        Task<int> Create(ContactDTO contact);
        Task<bool> Update(ContactUpdateDTO contact);
        Task<bool> DeleteContact(string id);
    }
}
