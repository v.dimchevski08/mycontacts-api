﻿using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyContactsApi.Service
{
    public interface IUserService
    {
        Task<List<UserDTO>> GetAll();
        Task<List<UserDTO>> GetActive();
        Task<UserDTO> GetUser(string id);
        Task<int> Create(UserDTO user);
        Task<bool> Update(UserUpdateDTO user);
        Task<bool> DeleteUser(string id);
    }
}
