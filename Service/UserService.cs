﻿using AutoMapper;
using DTOs;
using MyContactsApi.Models;
using MyContactsApi.Repo;
using MyContactsApi.Utilities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyContactsApi.Service
{
    public class UserService : IUserService
    {
        private readonly IMapper mapper;
        private readonly IUserRepo repo;

        public UserService(IMapper _mapper, IUserRepo _repo)
        {
            mapper = _mapper;
            repo = _repo;
        }

        #region POST
        public async Task<int> Create(UserDTO user)
        {
            return await repo.Create(mapper.Map<User>(user));
        }
        #endregion

        #region DELETE
        public async Task<bool> DeleteUser(string id)
        {
            return await repo.DeleteUser(int.Parse(EncryptionHelper.Decrypt(id)));
        }
        #endregion

        #region GET
        public async Task<List<UserDTO>> GetActive()
        {
            return mapper.Map<List<UserDTO>>(await repo.GetActive());
        }

        public async Task<List<UserDTO>> GetAll()
        {
            return mapper.Map<List<UserDTO>>(await repo.GetAll());
        }

        public async Task<UserDTO> GetUser(string id)
        {
            return mapper.Map<UserDTO>(await repo.GetUser(int.Parse(EncryptionHelper.Decrypt(id))));
        }
        #endregion

        #region PUT
        public async Task<bool> Update(UserUpdateDTO user)
        {
            return await repo.Update(mapper.Map<User>(user));
        }
        #endregion
    }
}
