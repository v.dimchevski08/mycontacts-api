﻿using AutoMapper;
using DTOs;
using MyContactsApi.Models;
using MyContactsApi.Repo;
using MyContactsApi.Utilities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyContactsApi.Service
{
    public class ContactService : IContactService
    {
        private readonly IMapper mapper;
        private readonly IContactRepo repo;

        public ContactService(IMapper _mapper, IContactRepo _repo)
        {
            mapper = _mapper;
            repo = _repo;
        }

        #region POST
        public async Task<int> Create(ContactDTO contact)
        {
            return await repo.Create(mapper.Map<Contact>(contact));
        }
        #endregion

        #region DELETE
        public async Task<bool> DeleteContact(string id)
        {
            return await repo.DeleteContact(int.Parse(EncryptionHelper.Decrypt(id)));
        }
        #endregion

        #region GET
        public async Task<List<ContactDTO>> GetUserContacts(string id)
        {
            return mapper.Map<List<ContactDTO>>(await repo.GetUserContacts(int.Parse(EncryptionHelper.Decrypt(id))));
        }

        public async Task<ContactDTO> GetContact(string id)
        {
            return mapper.Map<ContactDTO>(await repo.GetContact(int.Parse(EncryptionHelper.Decrypt(id))));
        }
        #endregion

        #region PUT
        public async Task<bool> Update(ContactUpdateDTO contact)
        {
            return await repo.Update(mapper.Map<Contact>(contact));
        }
        #endregion
    }
}
