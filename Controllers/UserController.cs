﻿using DTOs;
using Microsoft.AspNetCore.Mvc;
using MyContactsApi.Service;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace MyContactsApi.Controllers
{
    [Route("user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService service;
        private readonly ILogger<UserController> logger;

        public UserController(IUserService _service, ILogger<UserController> _logger)
        {
            service = _service;
            logger = _logger;
        }

        #region GET
        /// <summary>
        /// Get list of all users
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET user/list
        ///     
        /// 
        /// </remarks> 
        /// <returns>List of all users</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            if((service.GetAll().Result == null) || (!service.GetAll().Result.Any()))
            {
                logger.LogInformation("No data loaded because of a non-existing list.");
                return new NotFoundObjectResult("The list of users is non-existing.");
            }

            return new ObjectResult(await service.GetAll());
        }

        /// <summary>
        /// Get list of all active users
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET user/list/active
        ///     
        /// 
        /// </remarks>
        /// <returns>List of all active users</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpGet("list/active")]
        public async Task<IActionResult> GetActive()
        {
            if ((service.GetActive().Result == null) || (!service.GetActive().Result.Any()))
            {
                logger.LogInformation("No data loaded because of a non-existing list.");
                return new NotFoundObjectResult("The list of active users is non-existing.");
            }

            return new ObjectResult(await service.GetActive());
        }

        /// <summary>
        /// Get specific user by ID
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET user/id
        ///     
        /// 
        /// </remarks>
        /// <param name="id">ID of the specific user</param>
        /// <returns>Specific user</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(string id)
        {
            if (await service.GetUser(id) == null)
            {
                logger.LogInformation("No data loaded because of a non-existing user.");
                return new NotFoundObjectResult("The user is non-existing.");
            }

            return new ObjectResult(await service.GetUser(id));
        }
        #endregion

        #region POST
        /// <summary>
        /// Create new user
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST user/create
        ///     {
        ///         "firstName": "John",
        ///         "lastName": "Doe",
        ///         "email": "johndoe@email.com",
        ///         "phone": "+090192804",
        ///         "status": "Active"
        ///     }
        /// 
        /// *Mandatory Fields: All fields are mandatory
        /// </remarks>
        /// <param name="user">User's data</param>
        /// <returns>ID of the newly created user</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] UserDTO user)
        {
            if(string.IsNullOrEmpty(user.FirstName) || string.IsNullOrEmpty(user.LastName) || string.IsNullOrEmpty(user.Email) || string.IsNullOrEmpty(user.Phone) || string.IsNullOrEmpty(user.Status))
            {
                logger.LogWarning("Mandatory fields were not filled!");
                return new BadRequestObjectResult("Incorrect input. Please check the mandatory fields.");
            }

            return new ObjectResult(await service.Create(user));
        }
        #endregion

        #region PUT
        /// <summary>
        /// Update user
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST user/update
        ///     {
        ///         "userId": 24,
        ///         "firstName": "John",
        ///         "lastName": "Doe",
        ///         "email": "johndoe@email.com",
        ///         "phone": "+090192804",
        ///         "status": "Active"
        ///     }
        ///     
        /// *Mandatory Fields: All fields are mandatory
        /// </remarks>
        /// <param name="user">User's data</param>
        /// <returns>True if the request has succeeded, otherwise false</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] UserUpdateDTO user)
        {
            if(await service.GetUser(user.UserId) == null)
            {
                logger.LogInformation("An update to a non-existing user is impossible.");
                return new NotFoundObjectResult("The user is non-existing, hence could not be updated.");
            }

            if (string.IsNullOrEmpty(user.FirstName) || string.IsNullOrEmpty(user.LastName) || string.IsNullOrEmpty(user.Email) || string.IsNullOrEmpty(user.Phone) || string.IsNullOrEmpty(user.Status))
            {
                logger.LogWarning("Mandatory fields were not filled!");
                return new BadRequestObjectResult("Incorrect input. Please check the mandatory fields.");
            }

            return new ObjectResult(await service.Update(user));
        }
        #endregion

        #region DELETE
        /// <summary>
        /// Delete user
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST user/delete/9
        ///     
        /// *Mandatory Fields: User ID
        /// </remarks>
        /// <param name="id">ID of the specific user</param>
        /// <returns>True if the request has succeeded, otherwise false</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteUser(string id)
        {
            if(await service.GetUser(id) == null)
            {
                logger.LogInformation("Unable to delete a non-existing user.");
                return new NotFoundObjectResult("The user is non-existing, hence could not be deleted.");
            }

            return new ObjectResult(await service.DeleteUser(id));
        }
        #endregion
    }
}
