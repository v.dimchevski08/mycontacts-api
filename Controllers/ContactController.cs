﻿using DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyContactsApi.Service;
using System.Threading.Tasks;

namespace MyContactsApi.Controllers
{
    [Route("contact")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IContactService contactService;
        private readonly IUserService userService;
        private readonly ILogger<ContactController> logger;

        public ContactController(IContactService _contactService, 
                                 IUserService _userService, 
                                 ILogger<ContactController> _logger)
        {
            contactService = _contactService;
            userService = _userService;
            logger = _logger;
        }

        #region GET
        /// <summary>
        /// Get all contacts for specific user
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET contact/list/4
        ///     
        /// *Mandatory fields: User ID
        /// </remarks> 
        /// <param name="id">ID of the specific user</param>
        /// <returns>List of all contacts for specific user</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpGet("list")]
        public async Task<IActionResult> GetUserContacts(string id)
        {
            if(await userService.GetUser(id) == null)
            {
                logger.LogInformation("There is no contacts list for a non-existing user.");
                return new NotFoundObjectResult("The user is non-existing, hence there is no contacts list.");
            }

            return new ObjectResult(await contactService.GetUserContacts(id));
        }

        /// <summary>
        /// Get specific contact by ID
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET contact/id
        ///     
        /// 
        /// </remarks>
        /// <param name="id">ID of the specific contact</param>
        /// <returns>Specific contact</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetContact(string id)
        {
            if(await contactService.GetContact(id) == null)
            {
                logger.LogInformation("No data loaded because of a non-existing contact.");
                return new NotFoundObjectResult("The contact is non-existing.");
            }

            return new ObjectResult(await contactService.GetContact(id));
        }
        #endregion

        #region POST
        /// <summary>
        /// Create new contact
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST contact/create
        ///     {
        ///         "firstName": "Trevor",
        ///         "lastName": "Donovan",
        ///         "mobilePhone": "+29105729",
        ///         "homePhone": "+99175662",
        ///         "email": "trevor@donovan.com",
        ///         "address": "Freedom Square 4059",
        ///         "website": "www.dummywebsite.com",
        ///         "photo": "www.specificlinkfromthephoto.com",
        ///         "favorite": true,
        ///         "category": "Friends",
        ///         "userId": 57
        ///     }
        /// 
        /// *Mandatory Fields: First Name, Last Name, Mobile Phone, Email, User ID 
        /// 
        /// *Three categories available only: Friends, Family, Colleagues
        /// </remarks>
        /// <param name="contact">Contact's data</param>
        /// <returns>ID of the newly created contact</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        /// <response code="404">Indicates that the resource is non-existing</response>
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody] ContactDTO contact)
        {
            if(await userService.GetUser(contact.UserId) == null)
            {
                logger.LogInformation("A contact for a non-existing user can not be created.");
                return new NotFoundObjectResult("The user is non-existing, hence the contact could not be created.");
            }

            if(string.IsNullOrEmpty(contact.FirstName) || string.IsNullOrEmpty(contact.LastName) || string.IsNullOrEmpty(contact.MobilePhone) || string.IsNullOrEmpty(contact.Email))
            {
                logger.LogWarning("Mandatory fields were not filled!");
                return new BadRequestObjectResult("Incorrect input. Please check the mandatory fields.");
            }

            if(string.IsNullOrEmpty(contact.Category) || contact.Category == "Friends" || contact.Category == "Family" || contact.Category == "Colleagues")
            {
                return new ObjectResult(await contactService.Create(contact));
            }

            logger.LogWarning("A non-available category was chosen!");
            return new BadRequestObjectResult("If you assign a category, there are three available only: Family, Friends or Colleagues.");
        }
        #endregion

        #region PUT
        /// <summary>
        /// Update new contact
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST contact/update
        ///     {
        ///         "contactId": 43,
        ///         "firstName": "Trevor",
        ///         "lastName": "Donovan",
        ///         "mobilePhone": "+29105729",
        ///         "homePhone": "+99175662",
        ///         "email": "trevor@donovan.com",
        ///         "address": "Freedom Square 4059",
        ///         "website": "www.dummywebsite.com",
        ///         "photo": "www.specificlinkfromthephoto.com",
        ///         "favorite": true,
        ///         "category": "Friends",
        ///     }
        /// 
        /// *Mandatory Fields: First Name, Last Name, Mobile Phone, Email, Contact ID 
        /// 
        /// *Three categories available only: Friends, Family, Colleagues
        /// </remarks>
        /// <param name="contact">Contact's data</param>
        /// <returns>ID of the newly created contact</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] ContactUpdateDTO contact)
        {
            if(await contactService.GetContact(contact.ContactId) == null)
            {
                logger.LogInformation("An update to a non-existing contact is impossible.");
                return new NotFoundObjectResult("The contact is non-existing, hence could not be updated.");
            }

            if (string.IsNullOrEmpty(contact.FirstName) || string.IsNullOrEmpty(contact.LastName) || string.IsNullOrEmpty(contact.MobilePhone) || string.IsNullOrEmpty(contact.Email))
            {
                logger.LogWarning("Mandatory fields were not filled!");
                return new BadRequestObjectResult("Incorrect input. Please check the mandatory fields.");
            }

            if (string.IsNullOrEmpty(contact.Category) || contact.Category == "Friends" || contact.Category == "Family" || contact.Category == "Colleagues")
            {
                return new ObjectResult(await contactService.Update(contact));
            }

            logger.LogWarning("A non-available category was chosen!");
            return new BadRequestObjectResult("If you assign a category, there are three available only: Family, Friends or Colleagues.");
        }
        #endregion

        #region DELETE
        /// <summary>
        /// Delete contact
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST contact/delete/9
        ///     
        /// *Mandatory Fields: Contact ID
        /// </remarks>
        /// <param name="id">ID of the specific contact</param>
        /// <returns>True if the request has succeeded, otherwise false</returns>
        /// <response code="200">Indicates that the request has succeeded</response>
        /// <response code="400">Indicates that the input was incorrect</response>
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteContact(string id)
        {
            if (await contactService.GetContact(id) == null)
            {
                logger.LogInformation("Unable to delete a non-existing contact.");
                return new NotFoundObjectResult("The contact is non-existing, hence could not be deleted.");
            }

            return new ObjectResult(await contactService.DeleteContact(id));
        }
        #endregion
    }
}
