﻿using MyContactsApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyContactsApi.Repo
{
    public class UserRepo : IUserRepo
    {
        #region POST
        public async Task<int> Create(User user)
        {
            await using var db = new MyContactsContext();
            db.Users.Add(user);
            db.SaveChanges();

            return user.UserId;
        }
        #endregion

        #region DELETE
        public async Task<bool> DeleteUser(int id)
        {
            await using var db = new MyContactsContext();
            db.Users.Remove(await GetUser(id));
            db.SaveChanges();

            return true;
        }
        #endregion

        #region GET
        public async Task<List<User>> GetActive()
        {
            await using var db = new MyContactsContext();
            return db.Users.Where(u => u.Status == true).ToList();
        }

        public async Task<List<User>> GetAll()
        {
            await using var db = new MyContactsContext();
            return db.Users.ToList();
        }

        public async Task<User> GetUser(int id)
        {
            await using var db = new MyContactsContext();

            return db.Users.Where(u => u.UserId == id).FirstOrDefault();
        }
        #endregion

        #region PUT
        public async Task<bool> Update(User user)
        {
            await using var db = new MyContactsContext();

            User userUpdate = db.Users.Where(u => u.UserId == user.UserId).FirstOrDefault();

            userUpdate.FirstName = user.FirstName;
            userUpdate.LastName = user.LastName;
            userUpdate.Phone = user.Phone;
            userUpdate.Email = user.Email;
            userUpdate.Status = user.Status;

            db.SaveChanges();

            return true;
        }
        #endregion
    }
}
