﻿using MyContactsApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyContactsApi.Repo
{
    public interface IUserRepo
    {
        Task<List<User>> GetAll();
        Task<List<User>> GetActive();
        Task<User> GetUser(int id);
        Task<int> Create(User user);
        Task<bool> Update(User user);
        Task<bool> DeleteUser(int id);
    }
}
