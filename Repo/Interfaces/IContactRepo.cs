﻿using MyContactsApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyContactsApi.Repo
{
    public interface IContactRepo
    {
        Task<Contact> GetContact(int id);
        Task<List<Contact>> GetUserContacts(int id);
        Task<int> Create(Contact contact);
        Task<bool> Update(Contact contact);
        Task<bool> DeleteContact(int id);
    }
}
