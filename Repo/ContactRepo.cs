﻿using MyContactsApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyContactsApi.Repo
{
    public class ContactRepo : IContactRepo
    {
        #region POST
        public async Task<int> Create(Contact contact)
        {
            await using var db = new MyContactsContext();
            db.Add(contact);
            db.SaveChanges();

            return contact.ContactId;
        }
        #endregion

        #region DELETE
        public async Task<bool> DeleteContact(int id)
        {
            await using var db = new MyContactsContext();
            db.Remove(db.Contacts.Where(c => c.ContactId == id).FirstOrDefault());
            db.SaveChanges();

            return true;
        }
        #endregion

        #region GET
        public async Task<List<Contact>> GetUserContacts(int id)
        {
            await using var db = new MyContactsContext();

            return db.Contacts
                .OrderByDescending(c => c.Favorite)
                .ThenBy(c => c.Category)
                .Where(c => c.UserId == id)
                .ToList();
        }

        public async Task<Contact> GetContact(int id)
        {
            await using var db = new MyContactsContext();
            return db.Contacts.Where(c => c.ContactId == id).FirstOrDefault();
        }
        #endregion

        #region PUT
        public async Task<bool> Update(Contact contact)
        {
            await using var db = new MyContactsContext();

            Contact contactUpdate = db.Contacts.Where(c => c.ContactId == contact.ContactId).FirstOrDefault();

            contactUpdate.FirstName = contact.FirstName;
            contactUpdate.LastName = contact.LastName;
            contactUpdate.MobilePhone = contact.MobilePhone;
            contactUpdate.HomePhone = contact.HomePhone;
            contactUpdate.Email = contact.Email;
            contactUpdate.Address = contact.Address;
            contactUpdate.Website = contact.Website;
            contactUpdate.Photo = contact.Photo;
            contactUpdate.Favorite = contact.Favorite;
            contactUpdate.Category = contact.Category;

            db.SaveChanges();

            return true;
        }
        #endregion
    }
}
