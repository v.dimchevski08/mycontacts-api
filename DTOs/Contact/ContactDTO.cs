﻿namespace DTOs
{
    public class ContactDTO
    {
        public string ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        public string Photo { get; set; }
        public bool Favorite { get; set; }
        public string Category { get; set; }
        public string UserId { get; set; }
    }
}
