﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyContactsApi.Models
{
    public partial class User
    {
        public User()
        {
            Contacts = new HashSet<Contact>();
        }

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
