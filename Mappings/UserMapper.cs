﻿using AutoMapper;
using DTOs;
using MyContactsApi.Models;
using MyContactsApi.Utilities;

namespace MyContactsApi.Mappings
{
    public class UserMapper : Profile
    {
        public UserMapper()
        {
            CreateMap<User, UserDTO>()
                .ForMember(d => d.Status, config => config.MapFrom(s => s.Status == true ? "Active" : "Deactivated"))
                .ForMember(d => d.UserId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.UserId.ToString())));

            CreateMap<UserDTO, User>()
                .ForMember(d => d.Status, config => config.MapFrom(s => s.Status == "Active" ? true : false));

            CreateMap<UserUpdateDTO, User>()
                .ForMember(d => d.Status, config => config.MapFrom(s => s.Status == "Active" ? true : false))
                .ForMember(d => d.UserId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.UserId)));
        }
    }
}
