﻿using AutoMapper;
using DTOs;
using MyContactsApi.Models;
using MyContactsApi.Utilities;

namespace MyContactsApi.Mappings
{
    public class ContactMapper : Profile
    {
        public ContactMapper()
        {
            CreateMap<Contact, ContactDTO>()
                .ForMember(d => d.UserId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.UserId.ToString())))
                .ForMember(d => d.ContactId, config => config.MapFrom(s => EncryptionHelper.Encrypt(s.ContactId.ToString())));


            CreateMap<ContactDTO, Contact>()
                .ForMember(d => d.UserId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.UserId)));

            CreateMap<ContactUpdateDTO, Contact>()
                .ForMember(d => d.ContactId, config => config.MapFrom(s => EncryptionHelper.Decrypt(s.ContactId)));
        }
    }
}
